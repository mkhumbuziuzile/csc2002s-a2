# Makefile for AVL Data Structure
# Uzile Mkhumbuzi
# 26 April 20201

JAVAC=/usr/bin/javac
.SUFFIXES: .java .class
SRCDIR=src
BINDIR=bin

$(BINDIR)/%.class:$(SRCDIR)/%.java
	$(JAVAC) -d $(BINDIR)/ -cp $(BINDIR) $<

CLASSES=Score.class WordDictionary.class WordRecord.class Update.class Match.class WordPanel.class WordApp.class
CLASS_FILES=$(CLASSES:%.class=$(BINDIR)/%.class)

default: $(CLASS_FILES)

run:	$(CLASS_FILES)
	java -cp $(BINDIR) WordApp

clean:
	rm $(BINDIR)/*.class
	
GenerateDocs:	$(CLASS_FILES)
	javadocs/./generate

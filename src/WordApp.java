import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Scanner;
import java.util.concurrent.*;
//model is separate from the view.

public class WordApp {
//shared variables
	static int noWords=4;
	static int totalWords;

   	static int frameX=1000;
	static int frameY=600;
	static int yLimit=480;

	static WordDictionary dict = new WordDictionary(); //use default dictionary, to read from file eventually

	static WordRecord[] words;
	static volatile boolean done;  //must be volatile
	static 	Score score = new Score();

	static WordPanel w;
	
	
	
	public static void setupGUI(int frameX,int frameY,int yLimit) {
		// Frame init and dimensions
    	JFrame frame = new JFrame("WordGame"); 
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setSize(frameX, frameY);
      JPanel g = new JPanel();
      g.setLayout(new BoxLayout(g, BoxLayout.PAGE_AXIS)); 
      g.setSize(frameX,frameY);
    	
		w = new WordPanel(words,yLimit);
		w.passScore(score);
		w.setSize(frameX,yLimit+100);
	   g.add(w); 
	    
      JPanel txt = new JPanel();
      txt.setLayout(new BoxLayout(txt, BoxLayout.LINE_AXIS)); 
      JLabel caught =new JLabel("Caught: " + score.getCaught() + "    ");
      JLabel missed =new JLabel("Missed:" + score.getMissed()+ "    ");
      JLabel scr =new JLabel("Score:" + score.getScore()+ "    ");    
      txt.add(caught);
	   txt.add(missed);
	   txt.add(scr);
	   Update U = new Update(score,txt,scr,caught,missed);
	   Thread d2 =  new Thread(U);
	   d2.start();

	    //[snip]
  
	   final JTextField textEntry = new JTextField("",20);
	   textEntry.addActionListener(new ActionListener()
	   {
	      public void actionPerformed(ActionEvent evt) {
	         String text = textEntry.getText();
			 Match match = new Match(words, text, score);
			 match.setNumWords(noWords);
			 Thread d = new Thread(match);
			 d.start();
			 try {
				d.join();
			 }

			 catch (InterruptedException f)
			 	{
					System.out.println("Failed to join!!!");
					d.yield();
				}
			 
			 if (totalWords == score.getCaught())
			 	{
					JFrame over = new JFrame("Game completed");
					over.setSize(frameX, frameY/3);
					JLabel Tover = new JLabel("Reached the total number of words to catch!!! \n press quit to end or restart to restart game");
					over.add(Tover);
					over.setVisible(true);
					score.resetScore();
					w.timerStop();
					

				}
	         textEntry.setText("");
	         textEntry.requestFocus();
			 
	      }
	   });
	   
	   txt.add(textEntry);
	   txt.setMaximumSize( txt.getPreferredSize() );
	   g.add(txt);
	    
	   JPanel b = new JPanel();
      b.setLayout(new BoxLayout(b, BoxLayout.LINE_AXIS)); 
	   JButton startB = new JButton("Start");;

			// add the listener to the jbutton to handle the "pressed" event
		startB.addActionListener(new ActionListener()
		{
		   public void actionPerformed(ActionEvent e)
		   {	
			Thread c = new Thread(w);
			w.timerStart();
			c.start();
		    textEntry.requestFocus();  //return focus to the text entry field
		   }
		});
		JButton endB = new JButton("End");;
			
				// add the listener to the jbutton to handle the "pressed" event
		endB.addActionListener(new ActionListener()
		{
		   public void actionPerformed(ActionEvent e)
		   {	
				w.timerStop();
			   	for (int i=0;i<noWords;i++)
					words[i].resetWord();
			   score.resetScore();
			   w = null;
			   frame.dispose();
			   setupGUI(frameX, frameY, yLimit);



		   }
		});

		JButton quiB = new JButton("Quit");;
			
				// add the listener to the jbutton to handle the "pressed" event
		quiB.addActionListener(new ActionListener()
		{
		   public void actionPerformed(ActionEvent e)
		   {
			   System.exit(0);
		   }
		});
		
		b.add(startB);
		b.add(endB);
		b.add(quiB);
		
		g.add(b);
    	
      frame.setLocationRelativeTo(null);  // Center window on screen.
      frame.add(g); //add contents to window
      frame.setContentPane(g);     
       	//frame.pack();  // don't do this - packs it into small space
      frame.setVisible(true);
	}

   public static String[] getDictFromFile(String filename) {
		String [] dictStr = null;
		try {
			Scanner dictReader = new Scanner(new FileInputStream(filename));
			int dictLength = dictReader.nextInt();
			//System.out.println("read '" + dictLength+"'");

			dictStr=new String[dictLength];
			for (int i=0;i<dictLength;i++) {
				dictStr[i]=new String(dictReader.next());
				//System.out.println(i+ " read '" + dictStr[i]+"'"); //for checking
			}
			dictReader.close();
		} catch (IOException e) {
	        System.err.println("Problem reading file " + filename + " default dictionary will be used");
	    }
		return dictStr;
	}

	public static void main(String[] args) {
    	String[] tmpDict;
		if (args.length == 3)
			{
				totalWords=8;  //total words to fall
				noWords=5; // total words falling at any point
				assert(totalWords>=noWords); // this could be done more neatly
				tmpDict=getDictFromFile("u.txt"); //file of words
			}
		else
			{
				Scanner keyboard = new Scanner(System.in);
				System.out.println("Enter number of total words to fall: \n");
				totalWords=keyboard.nextInt();  //total words to fall
				System.out.println("Enter number of words to fall at a single point in time: \n");
				noWords=keyboard.nextInt(); // total words falling at any point
				System.out.println("Enter the name of the file having the words: \n");
				assert(totalWords>=noWords); // this could be done more neatly
				tmpDict=getDictFromFile(keyboard.next()); //file of words
			}

		if (tmpDict!=null)
			dict= new WordDictionary(tmpDict);
		
		WordRecord.dict=dict; //set the class dictionary for the words.
		
		words = new WordRecord[noWords];  //shared array of current words
		
		//[snip]
		
		setupGUI(frameX, frameY, yLimit);  
    	//Start WordPanel thread - for redrawing animation

		int x_inc=(int)frameX/noWords;
	  	//initialize shared array of current words

		for (int i=0;i<noWords;i++) {
			words[i]=new WordRecord(dict.getNewWord(),i*x_inc,yLimit);
		}
	}
}
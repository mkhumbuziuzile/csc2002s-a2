import javax.swing.JFrame;
import javax.swing.JPanel;
/**
*Class to continously update the score 
*Uzile Mkhumbuzi
*1.0
*@since 2 September 2021
*/
public class Match implements java.lang.Runnable
{
    private WordRecord[] words;
    private String match;
    private Score score;
    int numWords;
/**
* Constructor
*/ 
    public Match(WordRecord[] words, String match, Score score)
        {
            this.words = words;
            this.match = match;
            this.score = score;

        }
/**
* Method to update position of the word and dissapear once it reaches red zone
*/ 
    public void run()
        {   boolean right = false;
            for (int i=0;i<numWords;i++)
			{ right = words[i].matchWord(match);
                if (right)
                    {
                        score.caughtWord(match.length());
                        break;
                    }
            }
        }
/**
* Set the number of words falling at a given time
*/ 
    public void setNumWords(int numWords)
        {
            this.numWords = numWords;
        }
    
}
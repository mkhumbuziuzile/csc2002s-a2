
import javax.swing.*;
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
*Class to continously update the score 
*Uzile Mkhumbuzi
*1.0
*@since 2 September 2021
*/

public class Update implements Runnable,ActionListener {
    JLabel scr,caught, missed;
    JPanel txt;
    Score score;
    Timer tm = new Timer(50, this);
/**
* Constructor
*/ 

    public Update(Score score, JPanel txt, JLabel src, JLabel caught, JLabel missed)
        {
            this.score = score;
            this.scr = src;
            this.txt = txt;
            this.caught = caught;
            this.missed = missed;
            tm.start();
        }
/**
* Get new score and update the score panel
*/ 

    public synchronized void run()
        {
            caught.setText("Caught: " + score.getCaught() + "    ");
            missed.setText("Missed:" + score.getMissed()+ "    ");
            scr.setText("Score:" + score.getScore()+ "    ");    
			txt.repaint();
        }
/**
* Timed method
*/ 
    public void actionPerformed(ActionEvent e)
        {
            Thread c = new Thread(this);
            c.start();
        }
    
}

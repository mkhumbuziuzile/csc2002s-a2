import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.CountDownLatch;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;

import javax.swing.Timer;

//import jdk.javadoc.internal.tool.Start;

public class WordPanel extends JPanel implements java.lang.Runnable,ActionListener {
		public static volatile boolean done;
		private WordRecord[] words;
		private int noWords;
		private int maxY;
		private Score score;
		Timer tm = new Timer(50, this);   // sets repitiion timer to continously update WordPanel on GUI
		
		public void paintComponent(Graphics g) {
		    int width = getWidth();
		    int height = getHeight();
		    g.clearRect(0,0,width,height);
		    g.setColor(Color.red);
		    g.fillRect(0,maxY-10,width,height);

		    g.setColor(Color.black);
		    g.setFont(new Font("Helvetica", Font.PLAIN, 26));
		   //draw the words
		   //animation must be added 
		    for (int i=0;i<noWords;i++){	    	
		    	//g.drawString(words[i].getWord(),words[i].getX(),words[i].getY());	
		    	g.drawString(words[i].getWord(),words[i].getX(),words[i].getY()+20);  //y-offset for skeleton so that you can see the words	
		    }

		   
		  }
		
		WordPanel(WordRecord[] words, int maxY) {
			this.words=words; //will this work?
			noWords = words.length;
			done=false;
			this.maxY=maxY;		
		}
		
		// Update word posision and remove words on red zone, then repaint WordPanel on the GUI
		public void run() {

			for (int i=0;i<noWords;i++)
			{	if (words[i].dropped())
					{
						words[i].resetWord();
						score.missedWord();
					}
					 
				else
					{
						int Wy = words[i].getY();
						int Ws = words[i].getSpeed()/300+1;
						words[i].setY(Wy+Ws);
					}
			}
			repaint();
		}

		// Update GUI on separate thread after every 50ms
		public void actionPerformed(ActionEvent e)
			{
				Thread c = new Thread(this);
				c.start();
			}

			public synchronized void passScore(Score score)
            {
                this.score = score;
            }
		
		public synchronized void timerStart()
			{
				tm.start();
			}
		
		public synchronized void timerStop()
			{
				tm.stop();
			}
	}
